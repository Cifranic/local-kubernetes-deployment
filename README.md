# local-kubernetes-deployment

# notes: 
- Tested on Centos7 with VMware Fusion 10 
- Minimum Recommended Requirements per Centos7 node: 2.5gb Memory, 2 CPU cores 
- Run install as root

Recommended node deployment (each are a separate VM running Centos7):
- 1x Master 
- 2x Workers 

Other:
- Once set up, check out [dashboard installation steps](./dashboard/howToDashboard.md)


# BOTH MASTER and WORKERS: 
Create repo info 
```terminal
cat <<EOF > /etc/yum.repos.d/kubernetes.repo
[kubernetes]
name=Kubernetes
baseurl=https://packages.cloud.google.com/yum/repos/kubernetes-el7-x86_64
enabled=1
gpgcheck=1
repo_gpgcheck=1
gpgkey=https://packages.cloud.google.com/yum/doc/yum-key.gpg https://packages.cloud.google.com/yum/doc/rpm-package-key.gpg
EOF
```

# BOTH MASTER and WORKERS
Installs kubernetes services, and docker
```terminal
yum install -y kubelet kubeadm kubectl docker
systemctl enable kubelet
systemctl start kubelet
systemctl enable docker
systemctl start docker
```

# MASTER ONLY 
Expose needed ports for workers
```terminal
firewall-cmd --permanent --add-port=6443/tcp
firewall-cmd --permanent --add-port=443/tcp
firewall-cmd --permanent --add-port=2379-2380/tcp
firewall-cmd --permanent --add-port=10250/tcp
firewall-cmd --permanent --add-port=10251/tcp
firewall-cmd --permanent --add-port=10252/tcp
firewall-cmd --permanent --add-port=10255/tcp
firewall-cmd --reload
```

# WORKERS ONLY 
Expose needed ports for workers
```terminal
firewall-cmd --permanent --add-port=10251/tcp
firewall-cmd --permanent --add-port=10250/tcp
firewall-cmd --permanent --add-port=10255/tcp
firewall-cmd --reload
```

# BOTH MASTER and WORKERS
Ensure that packets are properly processed by IP tables during filtering and port forwarding:
```terminal
cat <<EOF > /etc/sysctl.d/k8s.conf
net.bridge.bridge-nf-call-ip6tables = 1
net.bridge.bridge-nf-call-iptables = 1
EOF
sysctl --system
```

# BOTH MASTER and WORKERS
Disable selinx
```terminal
setenforce 0
sed -i 's/^SELINUX=enforcing$/SELINUX=permissive/' /etc/selinux/config
```

# BOTH MASTER and WORKERS
Disable swap for kublet to work:
```terminal
sed -i '/swap/d' /etc/fstab
swapoff -a
```

# MASTER ONLY 
Create a cluster 
```terminal
sudo kubeadm init --pod-network-cidr=192.168.0.0/16
```


# MASTER ONLY 
Manage cluster with 'kube' user
```terminal
export KUBEADMIN='kube'
useradd $KUBEADMIN
mkdir -p /home/$KUBEADMIN/.kube
cp -i /etc/kubernetes/admin.conf /home/$KUBEADMIN/.kube/config
chown $KUBEADMIN:$KUBEADMIN /home/$KUBEADMIN/.kube/config
```
# MASTER ONLY
Switch to user kube, and Set up a pod network... this allow pods to talk cross node 
```terminal
su kube
kubectl create -f https://docs.projectcalico.org/manifests/tigera-operator.yaml
kubectl create -f https://docs.projectcalico.org/manifests/custom-resources.yaml
```


# MASTER ONLY
Master should be set up! To manage the cluser, switch to user kube, and run your typical kubernetes commands: 
```terminal
kubectl get nodes
kubectl get pods --all-namespaces -o wide
```

# MASTER ONLY 
Youll notice that there is only one node, but this will change once we add the workers to the master; get the token by issueing this command: 
```terminal
kubeadm token create --print-join-command
```


# WORKERS ONLY: 
SWITCH to workers, and paste the output of that command.. it will look similar to: 
```terminal
kubeadm join --discovery-token cfgrty.1234567890jyrfgd --discovery-token-ca-cert-hash sha256:1234..cdef 1.2.3.4:6443
```

You're cluster should be all good to go! For visuals, install the dashboard:  [here](./dashboard/howToDashboard.md)