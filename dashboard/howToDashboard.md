Create dashboard objects (in repo root):
```terminal
kubectl apply -f dashboard/dashboard.yaml
```

Create service account andf cluster role binding for dashboard authentication/authorization:
```terminal
kubectl apply -f dashboard/serviceAccountRBAC.yaml
```

Bind dashboard port, 8001 to your localhost:
```terminal
kubectl proxy
```

Open a browser and connect to dashboard: 
http://localhost:8001/api/v1/namespaces/kubernetes-dashboard/services/https:kubernetes-dashboard:/proxy/

Get Bearer token for authentication:
```terminal
kubectl -n kubernetes-dashboard describe secret $(kubectl -n kubernetes-dashboard get secret | grep admin-user | awk '{print $1}')
```

More resources:  https://kubernetes.io/docs/tasks/access-application-cluster/web-ui-dashboard/
